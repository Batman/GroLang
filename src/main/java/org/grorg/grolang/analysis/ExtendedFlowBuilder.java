package org.grorg.grolang.analysis;

import io.vavr.Tuple2;
import io.vavr.Tuple3;
import io.vavr.collection.Set;
import org.grorg.grolang.ast.*;
import org.grorg.grolang.ast.Boolean;
import org.grorg.grolang.ast.Function;
import org.grorg.grolang.ast.Integer;
import org.grorg.grolang.ast.info.Label;
import org.grorg.grolang.ast.visitor.AstVisitor;

import static io.vavr.API.*;
import static io.vavr.Predicates.instanceOf;

class ExtendedFlowBuilder implements AstVisitor<Void>
{
    private Set<Tuple3<Label, Label, String>> res;

    public Set<Tuple3<Label, Label, String>> build(Program program, Set<Tuple2<Label, Label>> toExtend)
    {
        this.res = toExtend.map(l -> Tuple(l._1, l._2, ""));
        visit(program);
        return this.res;
    }

    @Override
    public Void visit(ArithmeticExpression expression)
    {
        return null;
    }

    @Override
    public Void visit(Assignment assignment)
    {
        return null;
    }

    @Override
    public Void visit(BinaryArithmeticOperation arithmeticOperation)
    {
        return null;
    }

    @Override
    public Void visit(Boolean booleanExpression)
    {
        return null;
    }

    @Override
    public Void visit(BooleanExpression expression)
    {
        return null;
    }

    @Override
    public Void visit(ComparisonOperation comparisonOperation)
    {
        return null;
    }

    @Override
    public Void visit(Consumer consumer)
    {
        return null;
    }

    @Override
    public Void visit(Expression expression)
    {
        return null;
    }

    @Override
    public Void visit(Function function)
    {
        return null;
    }

    @Override
    public Void visit(If ifStmt)
    {
        res = res.map(l ->
        {
            if (l._1.equals(ifStmt.label()))
            {
                if (l._2.equals(ifStmt.body().head().label()))
                {
                    return Tuple(l._1, l._2, "yes");
                }
                else
                {
                    return Tuple(l._1, l._2, "no");
                }
            }
            else
            {
                return l;
            }
        });
        ifStmt.body().forEach(this::visit);
        return null;
    }

    @Override
    public Void visit(IfElse ifElse)
    {
        res = res.map(l ->
        {
            if (l._1.equals(ifElse.label()))
            {
                if (l._2.equals(ifElse.ifBody().head().label()))
                {
                    return Tuple(l._1, l._2, "yes");
                }
                else
                {
                    return Tuple(l._1, l._2, "no");
                }
            }
            else
            {
                return l;
            }
        });
        ifElse.ifBody().forEach(this::visit);
        ifElse.elseBody().forEach(this::visit);
        return null;
    }

    @Override
    public Void visit(Integer integer)
    {
        return null;
    }

    @Override
    public Void visit(NotBooleanExpression expression)
    {
        return null;
    }

    @Override
    public Void visit(Procedure<?> procedure)
    {
        procedure.body().forEach(this::visit);
        return null;
    }

    @Override
    public Void visit(ProcedureCall procedureCall)
    {
        final Label entry = procedureCall.entryLabel();
        final Label exit = procedureCall.exitLabel();
        res = res.map(t ->
        {
            if (t._1.equals(exit))
            {
                return Tuple(entry, t._2, "then");
            }
            else if (t._2.equals(exit))
            {
                return Tuple(t._1, entry, "return");
            }
            return t;
        });
        return null;
    }

    @Override
    public Void visit(Program program)
    {
        program.body().forEach(this::visit);
        program.procedures().forEach(this::visit);
        return null;
    }

    @Override
    public Void visit(Skip skip)
    {
        return null;
    }

    @Override
    public Void visit(Statement stmt)
    {
        Match(stmt).of(
                Case($(instanceOf(If.class)), this::visit),
                Case($(instanceOf(IfElse.class)), this::visit),
                Case($(instanceOf(While.class)), this::visit),
                Case($(instanceOf(ProcedureCall.class)), this::visit),
                Case($(), () -> null)
        );
        return null;
    }

    @Override
    public Void visit(UnaryArithmeticOperator unaryArithmeticOperator)
    {
        return null;
    }

    @Override
    public Void visit(Variable variable)
    {
        return null;
    }

    @Override
    public Void visit(VariableName variableName)
    {
        return null;
    }

    @Override
    public Void visit(While whileStmt)
    {
        res = res.map(l ->
        {
            if (l._1.equals(whileStmt.label()))
            {
                if (l._2.equals(whileStmt.body().head().label()))
                {
                    return Tuple(l._1, l._2, "yes");
                }
                else
                {
                    return Tuple(l._1, l._2, "no");
                }
            }
            else
            {
                return l;
            }
        });
        whileStmt.body().forEach(this::visit);
        return null;
    }
}
