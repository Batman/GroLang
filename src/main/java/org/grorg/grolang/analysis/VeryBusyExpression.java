package org.grorg.grolang.analysis;

import io.vavr.Tuple2;
import io.vavr.collection.HashSet;
import io.vavr.collection.Map;
import io.vavr.collection.Set;
import org.grorg.grolang.ast.*;
import org.grorg.grolang.ast.Integer;
import org.grorg.grolang.ast.info.Label;

import static io.vavr.API.*;
import static io.vavr.Patterns.$None;
import static io.vavr.Patterns.$Some;
import static io.vavr.Predicates.instanceOf;

public class VeryBusyExpression extends Analysis<ArithmeticExpression<?>>
{
    private Analyser analyser;

    public VeryBusyExpression(Analyser analyser)
    {
        this.analyser = analyser;
    }

    @Override
    boolean include(Set<ArithmeticExpression<?>> elem1, Set<ArithmeticExpression<?>> elem2)
    {
        return elem1.containsAll(elem2);
    }

    @Override
    Set<ArithmeticExpression<?>> union(Set<ArithmeticExpression<?>> elem1, Set<ArithmeticExpression<?>> elem2)
    {
        return elem1.intersect(elem2);
    }

    @Override
    Set<ArithmeticExpression<?>> defaultValue()
    {
        return this.analyser.arithmeticExpressions();
    }

    @Override
    Set<ArithmeticExpression<?>> firstValue()
    {
        return HashSet.empty();
    }

    @Override
    Set<Label> entry()
    {
        return this.analyser.finals();
    }

    @Override
    Set<Tuple2<Label, Label>> flow()
    {
        return this.analyser.reverseFlow();
    }

    @Override
    Set<ArithmeticExpression<?>> kill(Label label)
    {
        return Match(analyser.blocks().get(label)).of(
                Case($Some($()), (AbstractSyntaxTree ast) -> Match(ast).of(
                        Case($(instanceOf(Assignment.class)),
                                assignment -> Match(assignment.expression()).of(
                                        Case($(instanceOf(VariableName.class)), VeryBusyExpression::emptySet),
                                        Case($(instanceOf(Integer.class)), VeryBusyExpression::emptySet),
                                        Case($(), () -> expressionsThatContainsVariable(assignment.affectedVariable())))
                        ),
                        Case($(), VeryBusyExpression::emptySet))
                ),
                Case($None(), VeryBusyExpression::emptySet));
    }

    private Set<ArithmeticExpression<?>> expressionsThatContainsVariable(VariableName variable)
    {
        return analyser.arithmeticExpressions().filter(exp -> FreeVariable.freeVariable(exp).contains(variable));
    }

    @Override
    Set<ArithmeticExpression<?>> gen(Label label)
    {
        return Match(analyser.blocks().get(label)).of(
                Case($Some($()), (AbstractSyntaxTree ast) ->
                        Match(ast).of(
                                Case($(instanceOf(Assignment.class)), assignment -> gen(assignment.expression())),
                                Case($(instanceOf(BooleanExpression.class)), VeryBusyExpression::gen),
                                Case($(), VeryBusyExpression::emptySet)
                        )
                ),
                Case($None(), VeryBusyExpression::emptySet));
    }

    private static Set<ArithmeticExpression<?>> gen(BooleanExpression expression)
    {
        return Match(expression).of(
                Case($(instanceOf(ComparisonOperation.class)), comp -> gen(comp.left()).union(gen(comp.right()))),
                Case($(), VeryBusyExpression::emptySet));
    }

    private static Set<ArithmeticExpression<?>> gen(ArithmeticExpression expression)
    {
        return Match(expression).of(
                Case($(instanceOf(BinaryArithmeticOperation.class)), exp -> HashSet.of(exp)),
                Case($(instanceOf(UnaryArithmeticOperator.class)), exp -> HashSet.of(exp)),
                Case($(), VeryBusyExpression::emptySet));
    }

    private static Set<ArithmeticExpression<?>> emptySet()
    {
        return HashSet.empty();
    }

    @Override
    public AnalysisResult<ArithmeticExpression<?>> result()
    {
        Map<Label, Set<ArithmeticExpression<?>>> result = runAnalysis();
        return new AnalysisResult<>("VBE", mapTransferOnResult(result), result);
    }
}
