package org.grorg.grolang.analysis;

import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import org.grorg.grolang.ast.*;
import org.grorg.grolang.ast.info.Label;

import static io.vavr.API.*;
import static io.vavr.Patterns.$Cons;
import static io.vavr.Patterns.$Nil;
import static io.vavr.Predicates.instanceOf;

class Labels
{
    static Set<Label> labels(List<Statement> labels)
    {
        return Match(labels).of(
                Case($Nil(), HashSet::empty),
                Case($Cons($(), $()), (x, xs) -> labels(x).union(labels(xs)))
        );
    }

    static Set<Label> labels(Statement statement)
    {
        return Match(statement).of(
                Case($(instanceOf(If.class)), Labels::labels),
                Case($(instanceOf(IfElse.class)), Labels::labels),
                Case($(instanceOf(While.class)), Labels::labels),
                Case($(instanceOf(ProcedureCall.class)), Labels::labels),
                Case($(), (elem) -> HashSet.of(elem.label()))
        );
    }

    private static Set<Label> labels(If ifStmt)
    {
        return HashSet.of(ifStmt.label()).union(labels(ifStmt.body()));
    }

    private static Set<Label> labels(IfElse ifElse)
    {
        return HashSet.of(ifElse.label()).union(labels(ifElse.ifBody())).union(labels(ifElse.elseBody()));
    }

    private static Set<Label> labels(While whileStmt)
    {
        return HashSet.of(whileStmt.label()).union(labels(whileStmt.body()));
    }

    private static Set<Label> labels(ProcedureCall procedureCall)
    {
        return HashSet.of(procedureCall.entryLabel(), procedureCall.exitLabel());
    }

    static Set<Label> labels(Procedure<?> procedure)
    {
        return HashSet.of(procedure.entryLabel(), procedure.exitLabel()).union(labels(procedure.body()));
    }
}
