package org.grorg.grolang.analysis;

import io.vavr.Tuple3;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import org.grorg.grolang.ast.*;
import org.grorg.grolang.ast.Boolean;
import org.grorg.grolang.ast.Function;
import org.grorg.grolang.ast.Integer;
import org.grorg.grolang.ast.info.Label;
import org.grorg.grolang.ast.info.LabelGenerator;
import org.grorg.grolang.ast.visitor.AstVisitor;

import static io.vavr.API.*;
import static io.vavr.Predicates.instanceOf;

public final class RepresentationBuilder implements AstVisitor<StringBuilder>
{
    private Analyser analyser;

    public RepresentationBuilder(Analyser analyser)
    {
        this.analyser = analyser;
    }

    public String generateDot()
    {
        Set<Tuple3<Label, Label, String>> extendedFlow =
                new ExtendedFlowBuilder().build(analyser.program(), analyser.flow());

        return "digraph " + analyser.program().name().getOrElse(new Identifier("")).identifier() +
                " {\n" +
                analyser.program().procedures().map(this::visit).mkString() +
                visit(analyser.program()) +
                extendedFlow.foldLeft("", (prev, labels) ->
                    prev + "    " + labels._1() + " -> " + labels._2() + "[label=\"" + labels._3() + "\"];\n") +
                "}\n";
    }

    public String generateDotWithAnalysis(AnalysisResult<?>... results)
    {
        Set<Tuple3<Label, Label, String>> extendedFlow =
                new ExtendedFlowBuilder().build(analyser.program(), analyser.flow());

        StringBuilder analysisRepresentation = new StringBuilder();
        for (AnalysisResult result: results)
        {
            analysisRepresentation.append(visit(result));
        }

        return "digraph " + analyser.program().name().getOrElse(new Identifier("")).identifier() +
                " {\n" +
                analyser.program().procedures().map(this::visit).mkString() +
                visit(analyser.program()) +
                analysisRepresentation.toString() +
                extendedFlow.foldLeft("", (prev, labels) ->
                        prev + "    " + labels._1() + " -> " + labels._2() + "[label=\"" + labels._3() + "\"];\n") +
                "}\n";
    }

    private StringBuilder visit(AnalysisResult<?> result)
    {
        final List<Label> labels = result.entry().keySet().toList().sortBy(Label::toString);
        return new StringBuilder("    ")
                .append(LabelGenerator.newInstance().newLabel())
                .append("[shape=none, margin=0, label=<\n")
                .append("        <TABLE BORDER=\"0\" CELLBORDER=\"1\" CELLSPACING=\"0\" CELLPADDING=\"4\">\n")
                .append("          <TR>\n            <TD>label</TD>\n            <TD>")
                .append(result.analysisType())
                .append("entry(l)</TD>\n            <TD>")
                .append(result.analysisType())
                .append("exit(l)</TD>\n          </TR>")
                .append(labels.foldLeft(new StringBuilder(), (acc, label) ->
                        acc.append("          <TR>\n")
                                .append("            <TD>")
                                .append(label)
                                .append("</TD>\n            <TD>")
                                .append(result.entry().get(label).getOrElse(HashSet::empty).mkString("{",", ", "}"))
                                .append("</TD>\n            <TD>")
                                .append(result.exit().get(label).getOrElse(HashSet::empty).mkString("{", ", ", "}"))
                                .append("</TD>\n          </TR>")))
                .append("        </TABLE>>];\n");
    }

    private static String generateLabel(Label label, String labelContent)
    {
        return "    " + label.toString() + "[label=\"[" + labelContent + "]{" + label.toString() + "}\"];\n";
    }

    @Override
    public StringBuilder visit(ArithmeticExpression expression)
    {
        return Match(expression).of(
                Case($(instanceOf(BinaryArithmeticOperation.class)), this::visit),
                Case($(instanceOf(UnaryArithmeticOperator.class)), this::visit),
                Case($(instanceOf(VariableName.class)), this::visit),
                Case($(instanceOf(Integer.class)), this::visit)
        );
    }

    @Override
    public StringBuilder visit(Assignment assignment)
    {
        return new StringBuilder(
                generateLabel(assignment.label(),
                        assignment.affectedVariable().name().identifier() +
                                " := " +
                                visit(assignment.expression())));
    }

    @Override
    public StringBuilder visit(BinaryArithmeticOperation arithmeticOperation)
    {
        final StringBuilder builder = new StringBuilder();
        builder.append(visit(arithmeticOperation.left()));
        builder.append(Match(arithmeticOperation.operation()).of(
                Case($(BinaryArithmeticOperation.Operator.PLUS), " + "),
                Case($(BinaryArithmeticOperation.Operator.MINUS), " - "),
                Case($(BinaryArithmeticOperation.Operator.PRODUCT), " * "),
                Case($(BinaryArithmeticOperation.Operator.DIVISION), " / ")));
        builder.append(visit(arithmeticOperation.right()));
        return builder;
    }

    @Override
    public StringBuilder visit(Boolean booleanExpression)
    {
        return new StringBuilder(booleanExpression.value() ? "true" : "false");
    }

    @Override
    public StringBuilder visit(BooleanExpression expression)
    {
        return Match(expression).of(
                Case($(instanceOf(Boolean.class)), this::visit),
                Case($(instanceOf(ComparisonOperation.class)), this::visit),
                Case($(instanceOf(NotBooleanExpression.class)), this::visit));
    }

    @Override
    public StringBuilder visit(ComparisonOperation comparisonOperation)
    {
        return new StringBuilder(visit(comparisonOperation.left()))
                .append(Match(comparisonOperation.operation()).of(
                        Case($(ComparisonOperation.Operator.DIFFERENT), " != "),
                        Case($(ComparisonOperation.Operator.EQUAL), " == "),
                        Case($(ComparisonOperation.Operator.LOWER_THAN), " < "),
                        Case($(ComparisonOperation.Operator.LOWER_THAN_OR_EQUAL), " <= "),
                        Case($(ComparisonOperation.Operator.GREATER_THAN), " > "),
                        Case($(ComparisonOperation.Operator.GREATER_THAN_OR_EQUAL), " >= ")))
                .append(visit(comparisonOperation.right()));
    }

    @Override
    public StringBuilder visit(Consumer consumer)
    {
        return new StringBuilder(generateLabel(consumer.entryLabel(),
                "proc " +
                        consumer.name().identifier() +
                        "(" +
                        consumer.arguments().map(this::visit).mkString(", ") +
                        ") is"));
    }

    @Override
    public StringBuilder visit(Expression expression)
    {
        return Match(expression).of(
                Case($(instanceOf(ArithmeticExpression.class)), this::visit),
                Case($(instanceOf(BooleanExpression.class)), this::visit));
    }

    @Override
    public StringBuilder visit(Function function)
    {
        return new StringBuilder(generateLabel(function.entryLabel(),
                "proc " +
                function.name().identifier() +
                "(" +
                function.arguments()
                        .map(this::visit)
                        .mkString("", ", ", ", res ") +
                visit(function.returnVar()) +
                ") is"));
    }

    @Override
    public StringBuilder visit(If ifStmt)
    {

        final StringBuilder builder = new StringBuilder(
                generateLabel(ifStmt.label(), visit(ifStmt.condition()).toString()));

        ifStmt.body().forEach(statement -> builder.append(visit(statement)));

        return builder;
    }

    @Override
    public StringBuilder visit(IfElse ifElse)
    {
        final StringBuilder builder = new StringBuilder(
                generateLabel(ifElse.label(), visit(ifElse.condition()).toString()));

        ifElse.ifBody().forEach(statement -> builder.append(visit(statement)));
        ifElse.elseBody().forEach(statement -> builder.append(visit(statement)));

        return builder;
    }

    @Override
    public StringBuilder visit(Integer integer)
    {
        return new StringBuilder()
                .append(integer.value());
    }

    @Override
    public StringBuilder visit(NotBooleanExpression expression)
    {
        return new StringBuilder()
                .append("!")
                .append(visit(expression.expression()));
    }

    @Override
    public StringBuilder visit(Procedure<? extends Procedure> procedure)
    {
        StringBuilder builder;
        if (procedure instanceof Consumer)
        {
            builder = visit((Consumer) procedure);
        }
        else
        {
            builder = visit((Function) procedure);
        }
        procedure.body().forEach(statement -> builder.append(visit(statement)));
        return builder.append(generateLabel(procedure.exitLabel(), "end"));
    }

    @Override
    public StringBuilder visit(ProcedureCall procedureCall)
    {
        return new StringBuilder("    ")
                .append(procedureCall.entryLabel())
                .append("[label=\"[call ")
                .append(procedureCall.procedureName().identifier())
                .append("(")
                .append(procedureCall.arguments().map(this::visit).mkString(", "))
                .append(")]{")
                .append(procedureCall.entryLabel())
                .append(", ")
                .append(procedureCall.exitLabel())
                .append("}\"];\n");
    }

    @Override
    public StringBuilder visit(Program program)
    {
        final StringBuilder representation = new StringBuilder();
        program.body().forEach(statement -> representation.append(visit(statement)));
        return representation;
    }

    @Override
    public StringBuilder visit(Skip skip)
    {
        return new StringBuilder(generateLabel(skip.label(), "skip"));
    }

    @Override
    public StringBuilder visit(Statement stmt)
    {
        return Match(stmt).of(
                Case($(instanceOf(Assignment.class)), this::visit),
                Case($(instanceOf(If.class)), this::visit),
                Case($(instanceOf(IfElse.class)), this::visit),
                Case($(instanceOf(While.class)), this::visit),
                Case($(instanceOf(ProcedureCall.class)), this::visit),
                Case($(instanceOf(Skip.class)), this::visit));
    }

    @Override
    public StringBuilder visit(UnaryArithmeticOperator unaryArithmeticOperator)
    {
        return new StringBuilder().append(Match(unaryArithmeticOperator.operation()).of(
                Case($(UnaryArithmeticOperator.Operator.MINUS), "-")))
                .append(visit(unaryArithmeticOperator.expression()));
    }

    @Override
    public StringBuilder visit(Variable variable)
    {
        return new StringBuilder(Match(variable.type()).of(
                Case($(Type.INT), "int"),
                Case($(Type.BOOLEAN), "boolean")))
                .append(" ")
                .append(variable.identifier().identifier());
    }

    @Override
    public StringBuilder visit(VariableName variableName)
    {
        return new StringBuilder(variableName.name().identifier());
    }

    @Override
    public StringBuilder visit(While whileStmt)
    {
        final StringBuilder builder = new StringBuilder(generateLabel(whileStmt.label(),
                visit(whileStmt.condition()).toString()));
        whileStmt.body().forEach(statement -> builder.append(visit(statement)));

        return builder;
    }
}
