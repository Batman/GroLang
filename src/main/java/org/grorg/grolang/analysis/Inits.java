package org.grorg.grolang.analysis;

import io.vavr.collection.List;
import org.grorg.grolang.ast.Procedure;
import org.grorg.grolang.ast.ProcedureCall;
import org.grorg.grolang.ast.Statement;
import org.grorg.grolang.ast.info.Label;

class Inits
{
    static Label init(List<Statement> stmts)
    {
        return init(stmts.head());
    }

    static Label init(Statement statement)
    {
        if (statement instanceof ProcedureCall)
            return init((ProcedureCall) statement);

        return statement.label();
    }

    private static Label init(ProcedureCall procedureCall)
    {
        return procedureCall.entryLabel();
    }

    static Label init(Procedure<?> procedure)
    {
        return procedure.entryLabel();
    }
}
