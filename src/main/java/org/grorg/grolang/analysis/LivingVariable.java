package org.grorg.grolang.analysis;

import io.vavr.Tuple2;
import io.vavr.collection.HashSet;
import io.vavr.collection.Map;
import io.vavr.collection.Set;
import org.grorg.grolang.ast.AbstractSyntaxTree;
import org.grorg.grolang.ast.Assignment;
import org.grorg.grolang.ast.BooleanExpression;
import org.grorg.grolang.ast.VariableName;
import org.grorg.grolang.ast.info.Label;

import static io.vavr.API.*;
import static io.vavr.Patterns.$None;
import static io.vavr.Patterns.$Some;
import static io.vavr.Predicates.instanceOf;

public class LivingVariable extends Analysis<VariableName>
{
    private Analyser analyser;

    public LivingVariable(Analyser analyser)
    {
        this.analyser = analyser;
    }

    @Override
    boolean include(Set<VariableName> elem1, Set<VariableName> elem2)
    {
        return elem2.containsAll(elem1);
    }

    @Override
    Set<VariableName> union(Set<VariableName> elem1, Set<VariableName> elem2)
    {
        return elem1.union(elem2);
    }

    @Override
    Set<VariableName> defaultValue()
    {
        return HashSet.empty();
    }

    @Override
    Set<VariableName> firstValue()
    {
        return HashSet.empty();
    }

    @Override
    Set<Label> entry()
    {
        return analyser.finals();
    }

    @Override
    Set<Tuple2<Label, Label>> flow()
    {
        return analyser.reverseFlow();
    }

    @Override
    Set<VariableName> kill(Label label)
    {
        return Match(analyser.blocks().get(label)).of(
            Case($Some($()), (AbstractSyntaxTree ast) -> Match(ast).of(
                    Case($(instanceOf(Assignment.class)),
                            (Assignment assignment) -> HashSet.of(assignment.affectedVariable())),
                    Case($(), HashSet::empty))
            ),
            Case($None(), HashSet::empty));
    }

    @Override
    Set<VariableName> gen(Label label)
    {
        return Match(analyser.blocks().get(label)).of(
                Case($Some($()), (AbstractSyntaxTree ast) -> Match(ast).of(
                        Case($(instanceOf(Assignment.class)),
                                (Assignment assignment) -> FreeVariable.freeVariable(assignment.expression())),
                        Case($(instanceOf(BooleanExpression.class)),  FreeVariable::freeVariable),
                        Case($(), HashSet::empty))
                ),
                Case($None(), HashSet::empty));
    }

    @Override
    public AnalysisResult<VariableName> result()
    {
        Map<Label, Set<VariableName>> result = runAnalysis();
        return new AnalysisResult<>("LV", mapTransferOnResult(result), result);
    }
}
