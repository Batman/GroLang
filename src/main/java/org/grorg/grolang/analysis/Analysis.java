package org.grorg.grolang.analysis;

import io.vavr.Tuple2;
import io.vavr.collection.*;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.Set;
import org.grorg.grolang.ast.info.Label;

import java.util.function.Supplier;

import static io.vavr.API.*;
import static io.vavr.Patterns.*;

public abstract class Analysis<R>
{
    private Map<Label, Set<R>> analysis;
    private List<Tuple2<Label, Label>> work;

    abstract boolean include(Set<R> elem1, Set<R> elem2);
    abstract Set<R> union(Set<R> elem1, Set<R> elem2);
    abstract Set<R> defaultValue();
    abstract Set<R> firstValue();
    abstract Set<Label> entry();
    abstract Set<Tuple2<Label, Label>> flow();
    abstract Set<R> kill(Label label);
    abstract Set<R> gen(Label label);

    private Map<Label, Set<R>> transfer(Map<Label, Set<R>> analysis, Label label)
    {
        Supplier<Set<R>> emptySet = HashSet::empty;
        return analysis.put(label, Match(analysis.get(label)).of(
                Case($Some($()), (Set<R> expressions) -> expressions.diff(kill(label))),
                Case($None(), emptySet))
                .union(gen(label)));
    }

    private void init()
    {
        this.work = flow().toList();
        this.analysis = flow().foldLeft(HashMap.empty(), (prev, elem) ->
                prev.put(elem._1, defaultValue())
                        .put(elem._2, defaultValue()));
        this.analysis = entry().foldLeft(this.analysis, (prev, elem) -> prev.put(elem, firstValue()));
    }

    private void iter(List<Tuple2<Label, Label>> work)
    {
        Match(work).of(
                Case($Nil(), () -> null),
                Case($Cons($(), $()), (x, xs) ->
                {
                    this.work = xs;
                    Map<Label, Set<R>> transferredMap = transfer(this.analysis, x._1);
                    Set<R> transferred = transferredMap.getOrElse(x._1, HashSet.empty());
                    Set<R> next = this.analysis.getOrElse(x._2, HashSet.empty());
                    if (!include(transferred, next))
                    {
                        this.analysis = this.analysis.put(x._2, union(next, transferred));
                        this.work = flow().filter(t -> x._2.equals(t._1)).toList().appendAll(this.work);
                    }
                    iter(this.work);
                    return null;
                }));
    }

    public Map<Label, Set<R>> runAnalysis()
    {
        init();
        iter(this.work);
        return this.analysis;
    }

    Map<Label, Set<R>> mapTransferOnResult(Map<Label, Set<R>> result)
    {
        return result.map((label, set) ->
                Tuple(label, transfer(this.analysis, label).getOrElse(label, HashSet.empty())));
    }

    public abstract AnalysisResult<R> result();
}
