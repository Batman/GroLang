package org.grorg.grolang.analysis;

import io.vavr.collection.Map;
import io.vavr.collection.Set;
import org.grorg.grolang.ast.info.Label;

public class AnalysisResult<R>
{
    private String analysisType;
    private Map<Label, Set<R>> exit;
    private Map<Label, Set<R>> entry;

    public AnalysisResult(String analysisType, Map<Label, Set<R>> entry, Map<Label, Set<R>> exit)
    {
        this.analysisType = analysisType;
        this.entry = entry;
        this.exit = exit;
    }

    public Map<Label, Set<R>> entry()
    {
        return entry;
    }

    public Map<Label, Set<R>> exit()
    {
        return exit;
    }

    public String analysisType()
    {
        return this.analysisType;
    }
}
