package org.grorg.grolang.analysis;

import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import org.grorg.grolang.ast.*;
import org.grorg.grolang.ast.info.Label;

import static io.vavr.API.*;
import static io.vavr.Patterns.$Cons;
import static io.vavr.Patterns.$Nil;
import static io.vavr.Predicates.instanceOf;

public class Blocks
{
    static Map<Label, AbstractSyntaxTree<?>> blocks(List<Statement> stmts)
    {
        return emptyMap().merge(Match(stmts).of(
                Case($Nil(), HashMap::empty),
                Case($Cons($(), $(List.empty())), (x, xs) -> blocks(x)),
                Case($Cons($(), $()), (x, xs) -> blocks(x).merge(blocks(xs)))));
    }

    static Map<Label, AbstractSyntaxTree<?>> blocks(Statement stmt)
    {
        return emptyMap().merge(Match(stmt).of(
                Case($(instanceOf(If.class)), Blocks::blocks),
                Case($(instanceOf(IfElse.class)), Blocks::blocks),
                Case($(instanceOf(While.class)), Blocks::blocks),
                Case($(instanceOf(ProcedureCall.class)), Blocks::blocks),
                Case($(), (Statement<?> s) -> HashMap.of(s.label(), s))));
    }

    private static Map<Label, AbstractSyntaxTree<?>> blocks(If ifStmt)
    {
        return emptyMap().put(ifStmt.label(), ifStmt.condition())
                .merge(blocks(ifStmt.body()));
    }

    private static Map<Label, AbstractSyntaxTree<?>> blocks(IfElse ifElse)
    {
        return emptyMap().put(ifElse.label(), ifElse.condition())
                .merge(blocks(ifElse.ifBody()))
                .merge(blocks(ifElse.elseBody()));
    }

    private static Map<Label, AbstractSyntaxTree<?>> blocks(While whileStmt)
    {
        return emptyMap().put(whileStmt.label(), whileStmt.condition())
                .merge(blocks(whileStmt.body()));
    }

    private static Map<Label, AbstractSyntaxTree<?>> blocks(ProcedureCall call)
    {
        return emptyMap().put(call.entryLabel(), call).put(call.exitLabel(), call);
    }

    static Map<Label, AbstractSyntaxTree<?>> blocks(Procedure<?> procedure)
    {
        return emptyMap().put(procedure.begin().label(), procedure.begin())
                .put(procedure.end().label(), procedure.end())
                .merge(blocks(procedure.body()));
    }

    static Map<Label, AbstractSyntaxTree<?>> emptyMap()
    {
        return HashMap.empty();
    }
}
