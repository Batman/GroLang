package org.grorg.grolang.analysis;

import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import org.grorg.grolang.ast.*;

import static io.vavr.API.*;
import static io.vavr.Patterns.$Cons;
import static io.vavr.Patterns.$Nil;
import static io.vavr.Predicates.instanceOf;

/**
 * Check if a variable is used inside an expression
 */
final class FreeVariable
{
    private FreeVariable()
    {
        throw new InstantiationError("FreeVariable is not instantiable");
    }

    static Set<VariableName> freeVariable(List<Statement> stmts)
    {
        return Match(stmts).of(
                Case($Nil(), HashSet::empty),
                Case($Cons($(), $(List.empty())), (x, xs) -> freeVariable(x)),
                Case($Cons($(), $()), (x, xs) -> freeVariable(x).union(freeVariable(xs))));
    }

    static Set<VariableName> freeVariable(Statement stmt)
    {
        return Match(stmt).of(
                Case($(instanceOf(Assignment.class)), FreeVariable::freeVariable),
                Case($(instanceOf(If.class)), FreeVariable::freeVariable),
                Case($(instanceOf(IfElse.class)), FreeVariable::freeVariable),
                Case($(instanceOf(While.class)), FreeVariable::freeVariable),
                Case($(instanceOf(ProcedureCall.class)), FreeVariable::freeVariable),
                Case($(), FreeVariable::emptySet));
    }

    private static Set<VariableName> freeVariable(Assignment assignment)
    {
        return freeVariable(assignment.expression());
    }

    private static Set<VariableName> freeVariable(If ifStmt)
    {
        return freeVariable(ifStmt.condition())
                .union(freeVariable(ifStmt.body()));
    }

    private static Set<VariableName> freeVariable(IfElse ifElse)
    {
        return freeVariable(ifElse.condition())
                .union(freeVariable(ifElse.ifBody()))
                .union(freeVariable(ifElse.elseBody()));
    }

    private static Set<VariableName> freeVariable(While whileStmt)
    {
        return freeVariable(whileStmt.condition())
                .union(freeVariable(whileStmt.body()));
    }

    private static Set<VariableName> freeVariable(ProcedureCall call)
    {
        return call.arguments().map(FreeVariable::freeVariable).foldLeft(emptySet(), (prev, elem) -> prev.union(elem));
    }

    static Set<VariableName> freeVariable(Expression expression)
    {
        return Match(expression).of(
                Case($(instanceOf(ArithmeticExpression.class)), FreeVariable::freeVariable),
                Case($(instanceOf(BooleanExpression.class)), FreeVariable::freeVariable)
        );
    }

    private static Set<VariableName> freeVariable(ArithmeticExpression arithmeticExpression)
    {
        return Match(arithmeticExpression).of(
                Case($(instanceOf(BinaryArithmeticOperation.class)), FreeVariable::freeVariable),
                Case($(instanceOf(UnaryArithmeticOperator.class)), FreeVariable::freeVariable),
                Case($(instanceOf(VariableName.class)), var -> HashSet.of(var)),
                Case($(), FreeVariable::emptySet));
    }

    private static Set<VariableName> freeVariable(BinaryArithmeticOperation operation)
    {
        return freeVariable(operation.left()).union(freeVariable(operation.right()));
    }

    private static Set<VariableName> freeVariable(UnaryArithmeticOperator operation)
    {
        return freeVariable(operation.expression());
    }

    private static Set<VariableName> freeVariable(BooleanExpression booleanExpression)
    {
        return Match(booleanExpression).of(
                Case($(instanceOf(ComparisonOperation.class)), FreeVariable::freeVariable),
                Case($(instanceOf(NotBooleanExpression.class)), FreeVariable::freeVariable),
                Case($(), FreeVariable::emptySet)
        );
    }

    private static Set<VariableName> freeVariable(ComparisonOperation comparison)
    {
        return freeVariable(comparison.left()).union(freeVariable(comparison.right()));
    }

    private static Set<VariableName> freeVariable(NotBooleanExpression not)
    {
        return freeVariable(not.expression());
    }

    private static Set<VariableName> emptySet()
    {
        return HashSet.empty();
    }
}
