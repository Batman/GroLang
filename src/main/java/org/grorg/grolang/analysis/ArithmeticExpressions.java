package org.grorg.grolang.analysis;

import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import org.grorg.grolang.ast.*;

import java.util.function.Supplier;

import static io.vavr.API.*;
import static io.vavr.Patterns.$Cons;
import static io.vavr.Patterns.$Nil;
import static io.vavr.Predicates.instanceOf;


final class ArithmeticExpressions
{
    static Set<ArithmeticExpression<?>> arithmeticExpressions(List<Statement> stmts)
    {
        return Match(stmts).of(
            Case($Nil(), HashSet::empty),
            Case($Cons($(), $(List.empty())), (x, xs) -> arithmeticExpressions(x)),
            Case($Cons($(), $()), (x, xs) -> arithmeticExpressions(x).union(arithmeticExpressions(xs))));
    }

    static Set<ArithmeticExpression<?>> arithmeticExpressions(Statement stmt)
    {
        return Match(stmt).of(
                Case($(instanceOf(Assignment.class)), ArithmeticExpressions::arithmeticExpressions),
                Case($(instanceOf(If.class)), ArithmeticExpressions::arithmeticExpressions),
                Case($(instanceOf(IfElse.class)), ArithmeticExpressions::arithmeticExpressions),
                Case($(instanceOf(While.class)), ArithmeticExpressions::arithmeticExpressions),
                Case($(instanceOf(ProcedureCall.class)), ArithmeticExpressions::arithmeticExpressions),
                Case($(), HashSet::empty));
    }

    private static Set<ArithmeticExpression<?>> arithmeticExpressions(Assignment assignment)
    {
        return arithmeticExpressions(assignment.expression());
    }

    private static Set<ArithmeticExpression<?>> arithmeticExpressions(If ifStmt)
    {
        return arithmeticExpressions(ifStmt.condition())
                .union(arithmeticExpressions(ifStmt.body()));
    }

    private static Set<ArithmeticExpression<?>> arithmeticExpressions(IfElse ifElse)
    {
        return arithmeticExpressions(ifElse.condition())
                .union(arithmeticExpressions(ifElse.ifBody()))
                .union(arithmeticExpressions(ifElse.elseBody()));
    }

    private static Set<ArithmeticExpression<?>> arithmeticExpressions(While whileStmt)
    {
        return arithmeticExpressions(whileStmt.condition())
                .union(arithmeticExpressions(whileStmt.body()));
    }

    private static Set<ArithmeticExpression<?>> arithmeticExpressions(ProcedureCall call)
    {
        return call.arguments().foldLeft(HashSet.empty(), (prev, elem) -> prev.union(arithmeticExpressions(elem)));
    }

    private static Set<ArithmeticExpression<?>> arithmeticExpressions(BooleanExpression expression)
    {
        return Match(expression).of(
                Case($(instanceOf(ComparisonOperation.class)), ArithmeticExpressions::arithmeticExpressions),
                Case($(instanceOf(NotBooleanExpression.class)), ArithmeticExpressions::arithmeticExpressions),
                Case($(), HashSet::empty)
        );
    }

    private static Set<ArithmeticExpression<?>> arithmeticExpressions(ComparisonOperation comparisonOperation)
    {
        return arithmeticExpressions(comparisonOperation.right())
                .union(arithmeticExpressions(comparisonOperation.left()));
    }

    private static Set<ArithmeticExpression<?>> arithmeticExpressions(NotBooleanExpression expression)
    {
        return arithmeticExpressions(expression.expression());
    }


    private static Set<ArithmeticExpression<?>> arithmeticExpressions(ArithmeticExpression arithmeticExpression)
    {
        Supplier<Set<ArithmeticExpression<?>>> emptySet = HashSet::empty;
        return Match(arithmeticExpression).of(
                Case($(instanceOf(BinaryArithmeticOperation.class)), ArithmeticExpressions::arithmeticExpressions),
                Case($(instanceOf(UnaryArithmeticOperator.class)), ArithmeticExpressions::arithmeticExpressions),
                Case($(), emptySet));
    }

    private static Set<ArithmeticExpression<?>> arithmeticExpressions(BinaryArithmeticOperation operation)
    {
        return HashSet.of(operation);
    }

    private static Set<ArithmeticExpression<?>> arithmeticExpressions(UnaryArithmeticOperator operator)
    {
        return HashSet.of(operator);
    }

    static Set<ArithmeticExpression<?>> arithmeticExpressions(Procedure<?> procedure)
    {
        return arithmeticExpressions(procedure.body());
    }
}
