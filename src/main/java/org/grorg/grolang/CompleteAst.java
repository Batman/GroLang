package org.grorg.grolang;

import org.grorg.grolang.ast.*;
import org.grorg.grolang.ast.Boolean;
import org.grorg.grolang.ast.Function;
import org.grorg.grolang.ast.Integer;
import org.grorg.grolang.ast.visitor.AstVisitor;

import static io.vavr.API.*;
import static io.vavr.Predicates.instanceOf;

/**
 * Complete the Abstract Syntax Tree (program).
 * Add reference to procedure in procedureCall
 */
public class CompleteAst implements AstVisitor<Void>
{
    private Program program;

    public CompleteAst(Program program)
    {
        this.program = program;
    }

    public Program complete()
    {
        visit(program);
        return program;
    }

    @Override
    public Void visit(ArithmeticExpression expression)
    {
        return null;
    }

    @Override
    public Void visit(Assignment assignment)
    {
        return null;
    }

    @Override
    public Void visit(BinaryArithmeticOperation arithmeticOperation)
    {
        return null;
    }

    @Override
    public Void visit(Boolean booleanExpression)
    {
        return null;
    }

    @Override
    public Void visit(BooleanExpression expression)
    {
        return null;
    }

    @Override
    public Void visit(ComparisonOperation comparisonOperation)
    {
        return null;
    }

    @Override
    public Void visit(Consumer consumer)
    {
        return null;
    }

    @Override
    public Void visit(Expression expression)
    {
        return null;
    }

    @Override
    public Void visit(Function function)
    {
        return null;
    }

    @Override
    public Void visit(If ifStmt)
    {
        ifStmt.body().forEach(this::visit);
        return null;
    }

    @Override
    public Void visit(IfElse ifElse)
    {
        ifElse.ifBody().forEach(this::visit);
        ifElse.elseBody().forEach(this::visit);
        return null;
    }

    @Override
    public Void visit(Integer integer)
    {
        return null;
    }

    @Override
    public Void visit(NotBooleanExpression expression)
    {
        return null;
    }

    @Override
    public Void visit(Procedure<? extends Procedure> procedure)
    {
        procedure.body().forEach(this::visit);
        return null;
    }

    @Override
    public Void visit(ProcedureCall procedureCall)
    {
        procedureCall.setProcedure(
                program.procedures()
                        .find(procedure -> procedureCall.procedureName().equals(procedure.name())));
        return null;
    }

    @Override
    public Void visit(Program program)
    {
        program.procedures().forEach(this::visit);
        program.body().forEach(this::visit);
        return null;
    }

    @Override
    public Void visit(Skip skip)
    {
        return null;
    }

    @Override
    public Void visit(Statement stmt)
    {
        Match(stmt).of(
                Case($(instanceOf(If.class)), this::visit),
                Case($(instanceOf(IfElse.class)), this::visit),
                Case($(instanceOf(While.class)), this::visit),
                Case($(instanceOf(ProcedureCall.class)), this::visit),
                Case($(), () -> null)
        );
        return null;
    }

    @Override
    public Void visit(UnaryArithmeticOperator unaryArithmeticOperator)
    {
        return null;
    }

    @Override
    public Void visit(Variable variable)
    {
        return null;
    }

    @Override
    public Void visit(VariableName variableName)
    {
        return null;
    }

    @Override
    public Void visit(While whileStmt)
    {
        whileStmt.body().forEach(this::visit);
        return null;
    }
}
