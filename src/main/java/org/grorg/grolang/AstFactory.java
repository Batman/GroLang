package org.grorg.grolang;

import io.vavr.collection.List;
import org.antlr.v4.runtime.ParserRuleContext;
import org.grorg.grolang.ast.*;
import org.grorg.grolang.ast.Boolean;
import org.grorg.grolang.ast.Integer;
import org.grorg.grolang.ast.info.Info;
import org.grorg.grolang.ast.info.LabelGenerator;
import org.grorg.grolang.ast.info.Position;
import org.grorg.grolang.lang.GroLang;
import org.grorg.grolang.lang.GroLangBaseVisitor;

import static io.vavr.API.*;
import static io.vavr.Predicates.instanceOf;

public class AstFactory extends GroLangBaseVisitor<AbstractSyntaxTree<?>>
{
    private LabelGenerator generator = LabelGenerator.newInstance();

    private Info getInfo(ParserRuleContext ctx)
    {
        return new Info(
                new Position(ctx.start.getLine(), ctx.start.getCharPositionInLine()),
                new Position(ctx.stop.getLine(), ctx.stop.getCharPositionInLine()));
    }

    @Override
    public Program visitProgram(GroLang.ProgramContext ctx)
    {
        List<Procedure<?>> procedures = List.ofAll(ctx.declaration()).map(this::visitDeclaration);
        List<Variable> variables = visitVariableDeclarationList(ctx.variables);
        List<Statement> statements = visitStatementList(ctx.statements());
        Program program;
        if (ctx.programName != null)
        {
           program = new Program(new Identifier(ctx.programName.getText()), procedures, variables, statements);
        }
        else
        {
            program = new Program(procedures, variables, statements);
        }
        program.setInfo(getInfo(ctx));
        return new CompleteAst(program).complete();
    }

    @Override
    public Procedure<?> visitDeclaration(GroLang.DeclarationContext ctx)
    {
        Identifier procedureName = new Identifier(ctx.procedureName.getText());
        List<Variable> parameters = visitParameterDeclaration(ctx.parameters);
        List<Statement> statements = visitStatementList(ctx.stmt);
        if (ctx.returnType != null)
        {
            return new Function(generator.newLabel(), generator.newLabel(), procedureName, parameters, statements,
                    new Variable(
                            // if it passes syntax analysis it cannot be empty
                            Type.from(ctx.returnType.getText()).get(),
                            new Identifier(ctx.returnIdentifier.getText())
                    ))
                    .setInfo(getInfo(ctx));
        }
        return new Consumer(generator.newLabel(), generator.newLabel(), procedureName, parameters, statements)
                .setInfo(getInfo(ctx));
    }

    @Override
    @Deprecated
    public AbstractSyntaxTree<?> visitLDeclIdent(GroLang.LDeclIdentContext ctx)
    {
        throw new RuntimeException("Use visitParameterDeclaration instead");
    }

    private List<Variable> visitParameterDeclaration(GroLang.LDeclIdentContext ctx)
    {
        return List.ofAll(ctx.TYPE())
                .zip(ctx.IDENTIFIER())
                .map(typeIdentifier ->
                        new Variable(
                                // if it passes syntax analysis it cannot be empty
                                Type.from(typeIdentifier._1.getText()).get(),
                                new Identifier(typeIdentifier._2.getText()))
                                .setInfo(getInfo(ctx)));
    }

    @Override
    @Deprecated
    public AbstractSyntaxTree<?> visitLDeclVariables(GroLang.LDeclVariablesContext ctx)
    {
        throw new RuntimeException("Use visitVariableDeclarationList instead");
    }

    private List<Variable> visitVariableDeclarationList(GroLang.LDeclVariablesContext ctx)
    {
        if (ctx == null || ctx.head == null)
        {
            return List.empty();
        }

        return visitVariablesDeclaration(ctx.head).appendAll(visitVariableDeclarationList(ctx.tail));
    }

    @Override
    @Deprecated
    public AbstractSyntaxTree<?> visitDeclVariables(GroLang.DeclVariablesContext ctx)
    {
        throw new RuntimeException("use visitVariablesDeclaration instead");
    }

    private List<Variable> visitVariablesDeclaration(GroLang.DeclVariablesContext ctx)
    {
        return visitIdentifierList(ctx.identifiers)
                .map(identifier ->
                        new Variable(
                                Type.from(ctx.type.getText()).get(),
                                identifier)
                                .setInfo(getInfo(ctx)));
    }

    @Override
    @Deprecated
    public AbstractSyntaxTree<?> visitLIdentifier(GroLang.LIdentifierContext ctx)
    {
        throw new RuntimeException("use visitIdentifierList instead");
    }

    private List<Identifier> visitIdentifierList(GroLang.LIdentifierContext ctx)
    {
        return List.ofAll(ctx.IDENTIFIER()).map(identifier -> new Identifier(identifier.getText()));
    }

    @Override
    @Deprecated
    public AbstractSyntaxTree<?> visitBlock(GroLang.BlockContext ctx)
    {
        throw new RuntimeException("use visitBlockStmt instead");
    }

    private List<Statement> visitBlockStmt(GroLang.BlockContext ctx)
    {
        if (ctx.statement() != null)
        {
            return List.of(visitStatement(ctx.statement()));
        }
        else
        {
            return visitStatementList(ctx.statements());
        }
    }

    private Statement visitStatement(GroLang.StatementContext ctx)
    {
        return Match(ctx).of(
                Case($(instanceOf(GroLang.SkipContext.class)), this::visitSkip),
                Case($(instanceOf(GroLang.AssignmentContext.class)), this::visitAssignment),
                Case($(instanceOf(GroLang.IfContext.class)), this::visitIf),
                Case($(instanceOf(GroLang.WhileContext.class)), this::visitWhile),
                Case($(instanceOf(GroLang.FunctionCallContext.class)), this::visitFunctionCall));
    }

    @Override
    public AbstractSyntaxTree<?> visitStatements(GroLang.StatementsContext ctx)
    {
        throw new RuntimeException("Use visitStatementList instead");
    }

    private List<Statement> visitStatementList(GroLang.StatementsContext ctx)
    {
        if (ctx == null || ctx.head == null)
        {
            return List.empty();
        }

        return List.of(visitStatement(ctx.head))
                .appendAll(visitStatementList(ctx.tail));
    }

    @Override
    public Skip visitSkip(GroLang.SkipContext ctx)
    {
        return new Skip(generator.newLabel())
                .setInfo(getInfo(ctx));
    }

    @Override
    public Assignment visitAssignment(GroLang.AssignmentContext ctx)
    {
        return new Assignment(generator.newLabel(),
                new VariableName(new Identifier(ctx.identifier.getText())),
                visitArithmeticExpression(ctx.expr))
                .setInfo(getInfo(ctx));
    }

    @Override
    public Statement visitIf(GroLang.IfContext ctx)
    {
        if (ctx.elseBlock != null)
        {
            return new IfElse(
                    generator.newLabel(),
                    visitBooleanExpression(ctx.cond),
                    visitBlockStmt(ctx.ifBlock),
                    visitBlockStmt(ctx.elseBlock))
                    .setInfo(getInfo(ctx));
        }
        return new If(generator.newLabel(), visitBooleanExpression(ctx.cond), visitBlockStmt(ctx.ifBlock))
                .setInfo(getInfo(ctx));
    }

    @Override
    public While visitWhile(GroLang.WhileContext ctx)
    {
        return new While(generator.newLabel(), visitBooleanExpression(ctx.cond), visitBlockStmt(ctx.whileBlock))
                .setInfo(getInfo(ctx));
    }

    @Override
    public ProcedureCall visitFunctionCall(GroLang.FunctionCallContext ctx)
    {
        return new ProcedureCall(generator.newLabel(), generator.newLabel(), new Identifier(ctx.functionName.getText()),
                visitArithmeticExpressionList(ctx.arg))
                .setInfo(getInfo(ctx));
    }

    @Override
    @Deprecated
    public AbstractSyntaxTree<?> visitLAexpression(GroLang.LAexpressionContext ctx)
    {
        throw new RuntimeException("use visitArithmeticExpressionList instead");
    }

    private List<ArithmeticExpression> visitArithmeticExpressionList(GroLang.LAexpressionContext ctx)
    {
        return List.ofAll(ctx.aexpression()).map(this::visitArithmeticExpression);
    }

    private ArithmeticExpression visitArithmeticExpression(GroLang.AexpressionContext ctx)
    {
        return Match(ctx).of(
                Case($(instanceOf(GroLang.VarContext.class)), this::visitVar),
                Case($(instanceOf(GroLang.IntContext.class)), this::visitInt),
                Case($(instanceOf(GroLang.OpContext.class)), this::visitOp),
                Case($(instanceOf(GroLang.MinusOpContext.class)), this::visitMinusOp),
                Case($(instanceOf(GroLang.PriorityOpContext.class)), this::visitPriorityOp)
        );
    }

    @Override
    public BinaryArithmeticOperation visitOp(GroLang.OpContext ctx)
    {
        final ArithmeticExpression left = visitArithmeticExpression(ctx.left);
        final ArithmeticExpression right = visitArithmeticExpression(ctx.right);
        return Match(ctx.op.getText()).of(
                Case($("+"), BinaryArithmeticOperation.plus(left, right)),
                Case($("-"), BinaryArithmeticOperation.minus(left, right)),
                Case($("*"), BinaryArithmeticOperation.product(left, right)),
                Case($("/"), BinaryArithmeticOperation.division(left, right)))
                .setInfo(getInfo(ctx));
    }

    @Override
    public Boolean visitBool(GroLang.BoolContext ctx)
    {
        return new Boolean(java.lang.Boolean.valueOf(ctx.getText()))
                .setInfo(getInfo(ctx));
    }

    @Override
    public ArithmeticExpression visitPriorityOp(GroLang.PriorityOpContext ctx)
    {
        return visitArithmeticExpression(ctx.expr);
    }

    @Override
    public VariableName visitVar(GroLang.VarContext ctx)
    {
        return new VariableName(new Identifier(ctx.getText()))
                .setInfo(getInfo(ctx));
    }

    @Override
    public UnaryArithmeticOperator visitMinusOp(GroLang.MinusOpContext ctx)
    {
        return new UnaryArithmeticOperator(
                UnaryArithmeticOperator.Operator.MINUS,
                visitArithmeticExpression(ctx.expr))
                .setInfo(getInfo(ctx));
    }

    @Override
    public Integer visitInt(GroLang.IntContext ctx)
    {
        return new Integer(java.lang.Integer.valueOf(ctx.getText()))
                .setInfo(getInfo(ctx));
    }

    private BooleanExpression visitBooleanExpression(GroLang.BexpressionContext ctx)
    {
        return Match(ctx).of(
                Case($(instanceOf(GroLang.BoolContext.class)), this::visitBool),
                Case($(instanceOf(GroLang.ComparisonContext.class)), this::visitComparison),
                Case($(instanceOf(GroLang.NegateOpContext.class)), this::visitNegateOp),
                Case($(instanceOf(GroLang.BoolPriorityOpContext.class)), this::visitBoolPriorityOp)
        );
    }

    @Override
    public ComparisonOperation visitComparison(GroLang.ComparisonContext ctx)
    {
        final ArithmeticExpression left = visitArithmeticExpression(ctx.left);
        final ArithmeticExpression right = visitArithmeticExpression(ctx.right);
        return Match(ctx.op.getText()).of(
                Case($("<"), ComparisonOperation.lowerThan(left, right)),
                Case($("<="), ComparisonOperation.lowerThanOrEqual(left, right)),
                Case($(">"), ComparisonOperation.greaterThan(left, right)),
                Case($(">="), ComparisonOperation.greaterThanOrEqual(left, right)),
                Case($("="), ComparisonOperation.equal(left, right)),
                Case($("!="), ComparisonOperation.different(left, right)))
                .setInfo(getInfo(ctx));
    }

    @Override
    public NotBooleanExpression visitNegateOp(GroLang.NegateOpContext ctx)
    {
        return new NotBooleanExpression(visitBooleanExpression(ctx.expr))
                .setInfo(getInfo(ctx));
    }

    @Override
    public BooleanExpression visitBoolPriorityOp(GroLang.BoolPriorityOpContext ctx)
    {
        return visitBooleanExpression(ctx.expr);
    }
}
