package org.grorg.grolang.ast;

import io.vavr.collection.List;
import io.vavr.control.Option;
import org.grorg.grolang.ast.visitor.AstVisitor;

public final class Program extends AbstractSyntaxTree<Program>
{
    private Option<Identifier> name;
    private List<Procedure<?>> procedures;
    private List<Variable> variables;
    private List<Statement> body;

    public Program(List<Procedure<?>> procedures, List<Variable> variables, List<Statement> body)
    {
        this.name = Option.none();
        this.variables = variables;
        this.procedures = procedures;
        this.body = body;
    }

    public Program(Identifier name, List<Procedure<?>> procedures, List<Variable> variables, List<Statement> body)
    {
        this.name = Option.of(name);
        this.variables = variables;
        this.procedures = procedures;
        this.body = body;
    }

    public Option<Identifier> name()
    {
        return this.name;
    }

    public List<Procedure<?>> procedures()
    {
        return this.procedures;
    }

    public List<Statement> body()
    {
        return this.body;
    }

    @Override
    public <T> T accept(AstVisitor<T> visitor)
    {
        return visitor.visit(this);
    }

    @Override
    protected Program self()
    {
        return this;
    }
}
