package org.grorg.grolang.ast.info;

public final class Info
{
    private Position positionStart;
    private Position positionStop;

    public Info(Position positionStart, Position positionStop)
    {
        this.positionStart = positionStart;
        this.positionStop = positionStop;
    }

    public Position positionStart()
    {
        return this.positionStart;
    }

    public Position positionStop()
    {
        return positionStop;
    }
}
