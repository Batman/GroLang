package org.grorg.grolang.ast.info;

import java.util.Objects;

public class LabelGenerator
{
    private class LabelImpl implements Label
    {
        private LabelGenerator generator;
        private int label;

        private LabelImpl(LabelGenerator generator, int label)
        {
            this.generator = generator;
            this.label = label;
        }

        @Override
        public boolean equals(Object o)
        {
            // Two generator can generate same label, we must compare with generator too
            if (o instanceof Label)
            {
                return this.toString().equals(o.toString());
            }
            return false;
        }

        @Override
        public String toString()
        {
            return "l" + generator.id + "_" + label;
        }

        @Override
        public int hashCode()
        {
            return Objects.hash(toString());
        }
    }

    // The id of the last generator instance
    private static int instanceIndex = 0;
    private int id;
    private int index = 0;

    private LabelGenerator(int id)
    {
        this.id = id;
    }

    public static LabelGenerator newInstance()
    {
        return new LabelGenerator(++instanceIndex);
    }

    public Label newLabel()
    {
        return new LabelImpl(this, this.index++);
    }
}
