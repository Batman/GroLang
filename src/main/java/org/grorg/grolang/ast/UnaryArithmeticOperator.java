package org.grorg.grolang.ast;

import org.grorg.grolang.ast.visitor.AstVisitor;

import java.util.Objects;

public final class UnaryArithmeticOperator extends ArithmeticExpression<UnaryArithmeticOperator>
{
    public enum Operator
    {
        MINUS
    }

    private Operator operation;
    private ArithmeticExpression expression;

    public UnaryArithmeticOperator(Operator operation, ArithmeticExpression expression)
    {
        this.operation = operation;
        this.expression = expression;
    }

    public Operator operation()
    {
        return operation;
    }

    public ArithmeticExpression expression()
    {
        return expression;
    }

    @Override
    public <ReturnType> ReturnType accept(AstVisitor<ReturnType> visitor)
    {
        return visitor.visit(this);
    }

    @Override
    protected UnaryArithmeticOperator self()
    {
        return this;
    }

    public static UnaryArithmeticOperator minus(ArithmeticExpression expression)
    {
        return new UnaryArithmeticOperator(Operator.MINUS, expression);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        UnaryArithmeticOperator that = (UnaryArithmeticOperator) o;
        return operation == that.operation &&
                Objects.equals(expression, that.expression);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(operation, expression);
    }

    @Override
    public String toString()
    {
        return "-" + expression.toString();
    }
}
