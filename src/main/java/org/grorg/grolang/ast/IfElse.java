package org.grorg.grolang.ast;

import io.vavr.collection.List;
import org.grorg.grolang.ast.info.Label;
import org.grorg.grolang.ast.visitor.AstVisitor;

public final class IfElse extends Statement<IfElse>
{
    private Label label;
    private BooleanExpression cond;
    private List<Statement> ifBody;
    private List<Statement> elseBody;

    public IfElse(Label label, BooleanExpression cond, List<Statement> ifBody, List<Statement> elseBody)
    {
        this.label = label;
        this.cond = cond;
        this.ifBody = ifBody;
        this.elseBody = elseBody;
    }

    @Override
    public Label label()
    {
        return this.label;
    }

    public BooleanExpression condition()
    {
        return this.cond;
    }

    public List<Statement> ifBody()
    {
        return this.ifBody;
    }

    public List<Statement> elseBody()
    {
        return this.elseBody;
    }

    @Override
    public <ReturnType> ReturnType accept(AstVisitor<ReturnType> visitor)
    {
        return visitor.visit(this);
    }

    @Override
    protected IfElse self()
    {
        return this;
    }
}
