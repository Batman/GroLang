package org.grorg.grolang.ast;

import org.grorg.grolang.ast.info.Label;
import org.grorg.grolang.ast.visitor.AstVisitor;

public class ProcedureBegin extends Statement<ProcedureBegin>
{
    private Label label;

    public ProcedureBegin(Label label)
    {
        this.label = label;
    }

    @Override
    public Label label()
    {
        return label;
    }

    @Override
    protected ProcedureBegin self()
    {
        return this;
    }

    @Override
    public <ReturnType> ReturnType accept(AstVisitor<ReturnType> visitor)
    {
        return visitor.visit(this);
    }
}
