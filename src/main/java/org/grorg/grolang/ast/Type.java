package org.grorg.grolang.ast;

import io.vavr.control.Option;

public enum Type
{
    INT,
    BOOLEAN;

    public static Option<Type> from(String type)
    {
        switch (type)
        {
            case "int":
                return Option.of(INT);
            case "boolean":
                return Option.of(BOOLEAN);
            default:
                return Option.none();
        }
    }
}
