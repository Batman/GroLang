package org.grorg.grolang.ast;

import org.grorg.grolang.ast.visitor.AstVisitor;

import java.util.Objects;

public final class BinaryArithmeticOperation extends ArithmeticExpression<BinaryArithmeticOperation>
{
    public enum Operator
    {
        PLUS,
        MINUS,
        PRODUCT,
        DIVISION
    }

    private Operator operation;
    private ArithmeticExpression left;
    private ArithmeticExpression right;

    public BinaryArithmeticOperation(Operator operation, ArithmeticExpression left, ArithmeticExpression right)
    {
        this.operation = operation;
        this.left = left;
        this.right = right;
    }

    public Operator operation()
    {
        return operation;
    }

    public ArithmeticExpression left()
    {
        return left;
    }

    public ArithmeticExpression right()
    {
        return right;
    }

    @Override
    public <ReturnType> ReturnType accept(AstVisitor<ReturnType> visitor)
    {
        return visitor.visit(this);
    }

    @Override
    protected BinaryArithmeticOperation self()
    {
        return this;
    }

    public static BinaryArithmeticOperation plus(ArithmeticExpression left, ArithmeticExpression right)
    {
        return new BinaryArithmeticOperation(Operator.PLUS, left, right);
    }

    public static BinaryArithmeticOperation minus(ArithmeticExpression left, ArithmeticExpression right)
    {
        return new BinaryArithmeticOperation(Operator.MINUS, left, right);
    }

    public static BinaryArithmeticOperation product(ArithmeticExpression left, ArithmeticExpression right)
    {
        return new BinaryArithmeticOperation(Operator.PRODUCT, left, right);
    }

    public static BinaryArithmeticOperation division(ArithmeticExpression left, ArithmeticExpression right)
    {
        return new BinaryArithmeticOperation(Operator.DIVISION, left, right);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        BinaryArithmeticOperation that = (BinaryArithmeticOperation) o;

        return operation == that.operation &&
                // for + or * that are commutative
                ((operation == Operator.PLUS || operation == Operator.PRODUCT) &&
                        (Objects.equals(left, that.left) && Objects.equals(right, that.right)) ||
                        (Objects.equals(left, that.right) && Objects.equals(right, that.left)))
                ||
                // for - or / that are not
                Objects.equals(left, that.left) &&
                        Objects.equals(right, that.right);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(operation, left, right);
    }

    @Override
    public String toString()
    {
        final StringBuilder builder = new StringBuilder(this.left.toString());
        switch (this.operation())
        {

            case PLUS:
                builder.append(" + ");
                break;
            case MINUS:
                builder.append(" - ");
                break;
            case PRODUCT:
                builder.append(" * ");
                break;
            case DIVISION:
                builder.append(" / ");
                break;
        }

        return builder.append(this.right().toString()).toString();
    }
}
