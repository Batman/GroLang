package org.grorg.grolang.ast;

import io.vavr.collection.List;
import org.grorg.grolang.ast.info.Label;
import org.grorg.grolang.ast.visitor.AstVisitor;

public final class If extends Statement<If>
{
    private Label label;
    private BooleanExpression condition;
    private List<Statement> body;

    public If(Label label, BooleanExpression condition, List<Statement> body)
    {
        this.label = label;
        this.condition = condition;
        this.body = body;
    }

    @Override
    public Label label()
    {
        return this.label;
    }

    public BooleanExpression condition()
    {
        return condition;
    }

    public List<Statement> body()
    {
        return this.body;
    }

    @Override
    public <ReturnType> ReturnType accept(AstVisitor<ReturnType> visitor)
    {
        return visitor.visit(this);
    }

    @Override
    protected If self()
    {
        return this;
    }
}
