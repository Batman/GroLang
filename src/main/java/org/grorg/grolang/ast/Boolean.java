package org.grorg.grolang.ast;

import org.grorg.grolang.ast.visitor.AstVisitor;

import java.util.Objects;

public final class Boolean extends BooleanExpression<Boolean>
{
    private boolean value;

    public Boolean(boolean value)
    {
        this.value = value;
    }

    public boolean value()
    {
        return this.value;
    }

    @Override
    public <ReturnType> ReturnType accept(AstVisitor<ReturnType> visitor)
    {
        return visitor.visit(this);
    }

    @Override
    protected Boolean self()
    {
        return this;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        Boolean that = (Boolean) o;
        return this.value == that.value;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(value);
    }
}
