package org.grorg.grolang.ast;

import org.grorg.grolang.ast.visitor.AstVisitor;

public abstract class BooleanExpression<T extends BooleanExpression<T>> extends Expression<T>
{
    @Override
    public <ReturnType> ReturnType accept(AstVisitor<ReturnType> visitor)
    {
        return visitor.visit(this);
    }
}
