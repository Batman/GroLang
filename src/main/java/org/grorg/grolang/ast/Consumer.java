package org.grorg.grolang.ast;

import io.vavr.collection.List;
import org.grorg.grolang.ast.info.Label;
import org.grorg.grolang.ast.visitor.AstVisitor;

public final class Consumer extends Procedure<Consumer>
{
    public Consumer(Label entryLabel, Label exitLabel, Identifier name, List<Variable> arguments, List<Statement> body)
    {
        super(entryLabel, exitLabel, name, arguments, body);
    }

    @Override
    public <ReturnType> ReturnType accept(AstVisitor<ReturnType> visitor)
    {
        return visitor.visit(this);
    }

    @Override
    protected Consumer self()
    {
        return this;
    }
}
