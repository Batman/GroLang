package org.grorg.grolang.ast;

import org.grorg.grolang.ast.visitor.AstVisitor;

public final class Variable extends AbstractSyntaxTree<Variable>
{
    private Type type;
    private Identifier identifier;

    public Variable(Type type, Identifier identifier)
    {
        this.type = type;
        this.identifier = identifier;
    }

    public Type type()
    {
        return type;
    }

    public Identifier identifier()
    {
        return identifier;
    }

    @Override
    public <ReturnType> ReturnType accept(AstVisitor<ReturnType> visitor)
    {
        return visitor.visit(this);
    }

    @Override
    protected Variable self()
    {
        return this;
    }
}
