package org.grorg.grolang.ast;

import org.grorg.grolang.ast.info.Label;
import org.grorg.grolang.ast.visitor.AstVisitor;

public final class Assignment extends Statement<Assignment>
{
    private Label label;
    private VariableName affectedVariable;
    private ArithmeticExpression expression;

    public Assignment(Label label, VariableName affectedVariable, ArithmeticExpression expression)
    {
        this.label = label;
        this.affectedVariable = affectedVariable;
        this.expression = expression;
    }

    @Override
    public Label label()
    {
        return label;
    }

    public VariableName affectedVariable()
    {
        return affectedVariable;
    }

    public ArithmeticExpression expression()
    {
        return this.expression;
    }

    @Override
    public <ReturnType> ReturnType accept(AstVisitor<ReturnType> visitor)
    {
        return visitor.visit(this);
    }

    @Override
    protected Assignment self()
    {
        return this;
    }
}
