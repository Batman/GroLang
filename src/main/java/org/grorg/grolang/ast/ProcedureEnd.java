package org.grorg.grolang.ast;

import org.grorg.grolang.ast.info.Label;
import org.grorg.grolang.ast.visitor.AstVisitor;

public class ProcedureEnd extends Statement<ProcedureEnd>
{
    private Label label;

    public ProcedureEnd(Label label)
    {
        this.label = label;
    }

    @Override
    public Label label()
    {
        return label;
    }

    @Override
    protected ProcedureEnd self()
    {
        return this;
    }

    @Override
    public <ReturnType> ReturnType accept(AstVisitor<ReturnType> visitor)
    {
        return visitor.visit(this);
    }
}
