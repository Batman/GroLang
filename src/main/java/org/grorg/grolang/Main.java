package org.grorg.grolang;

import io.vavr.collection.Map;
import io.vavr.collection.Set;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.grorg.grolang.analysis.Analyser;
import org.grorg.grolang.analysis.AvailableExpression;
import org.grorg.grolang.analysis.RepresentationBuilder;
import org.grorg.grolang.ast.ArithmeticExpression;
import org.grorg.grolang.ast.Program;
import org.grorg.grolang.ast.info.Label;
import org.grorg.grolang.lang.GroLang;
import org.grorg.grolang.lang.GroLangLexer;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public final class Main
{
    public static void main(String... args)
    {
        final String p = Programs.p6;
        GroLangLexer lexer = new GroLangLexer(CharStreams.fromString(p));
        CommonTokenStream tokenStream = new CommonTokenStream(lexer);
        GroLang parser = new GroLang(tokenStream);

        GroLang.ProgramContext ctx = parser.program();
        Program program = new AstFactory().visitProgram(ctx);
        Analyser analyser = new Analyser(program);
        AvailableExpression a = new AvailableExpression(analyser);
        Map<Label, Set<ArithmeticExpression<?>>> analysis = a.runAnalysis();
        // final String representation = new RepresentationBuilder(analyser).generateDot();
        final AvailableExpression availableExpression = new AvailableExpression(analyser);
        final String representation = new RepresentationBuilder(analyser)
                .generateDotWithAnalysis(availableExpression.result());
        if (args.length > 0)
        {
            try
            {
                try (PrintWriter writer = new PrintWriter(args[0]))
                {
                    writer.println(representation);
                    System.out.println(representation);
                }
            }
            catch (FileNotFoundException e)
            {
                System.out.println(representation);
            }
        }
        else
        {
            System.out.println(representation);
        }
    }
}
