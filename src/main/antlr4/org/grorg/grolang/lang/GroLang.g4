parser grammar GroLang;

options
{
    language = Java;
    tokenVocab = GroLangLexer;
}

// synthaxe
program:
    PROGRAM_KEYWORD programName=IDENTIFIER? procedure=declaration*
    BEGIN_KEYWORD
       variables=lDeclVariables stmts=statements
    END_KEYWORD
;
declaration:
   PROCEDURE_KEYWORD procedureName=IDENTIFIER OPENING_PARENTHESIS parameters=lDeclIdent
       (SEPARATOR RESULT_KEYWORD returnType=TYPE returnIdentifier=IDENTIFIER)? CLOSING_PARENTHESIS
   BEGIN_KEYWORD
       stmt=statements
   END_KEYWORD
;
lDeclIdent: TYPE IDENTIFIER (SEPARATOR TYPE IDENTIFIER)*
;
lDeclVariables: head=declVariables tail=lDeclVariables*
;
declVariables: type=TYPE identifiers=lIdentifier END_OF_STMT
;
lIdentifier: IDENTIFIER (SEPARATOR IDENTIFIER)*
;
block:
      statement
    | OPENING_PARENTHESIS statements CLOSING_PARENTHESIS
;
statements: head=statement (END_OF_STMT tail=statements)*
;
statement:
      SKIP_KEYWORD                                        # skip
    | identifier=IDENTIFIER ASSIGN_OP expr=aexpression    # assignment
    | IF_KEYWORD cond=bexpression
      THEN_KEYWORD
          ifBlock=block
      (ELSE_KEYWORD elseBlock=block)?                     # if
    | WHILE_KEYWORD cond=bexpression
      DO_KEYWORD
          whileBlock=block                                # while
    | CALL_KEYWORD functionName=IDENTIFIER OPENING_PARENTHESIS arg=lAexpression CLOSING_PARENTHESIS # functionCall
;
lAexpression: head=aexpression (SEPARATOR tail=aexpression)*
;
aexpression:
      IDENTIFIER                                                  # var
    | INTEGER                                                     # int
    | left=aexpression op=PRODUCT_OP right=aexpression            # op
    | left=aexpression op=DIVISION_OP right=aexpression           # op
    | left=aexpression op=MINUS_OP right=aexpression              # op
    | left=aexpression op=PLUS_OP right=aexpression               # op
    | op=MINUS_OP expr=aexpression                                # minusOp
    | OPENING_PARENTHESIS expr=aexpression CLOSING_PARENTHESIS    # priorityOp
;
bexpression:
      BOOLEAN                                                      # bool
    | left=aexpression op=COMPARISON_OPERATOR right=aexpression    # comparison
    | op=NOT_KEYWORD expr=bexpression                              # negateOp
    | OPENING_PARENTHESIS expr=bexpression CLOSING_PARENTHESIS     # boolPriorityOp
;