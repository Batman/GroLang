lexer grammar GroLangLexer;

WS: [ \t\r\n]+ -> skip;

PROGRAM_KEYWORD: 'program';
PROCEDURE_KEYWORD: 'proc';
BEGIN_KEYWORD: 'begin';
END_KEYWORD: 'end';
RESULT_KEYWORD: 'res';

SKIP_KEYWORD: 'skip';
IF_KEYWORD: 'if';
THEN_KEYWORD: 'then';
ELSE_KEYWORD: 'else';
WHILE_KEYWORD: 'while';
DO_KEYWORD: 'do';
CALL_KEYWORD: 'call';
NOT_KEYWORD: 'not';

COMPARISON_OPERATOR: LT_OP | HT_OP | LTE_OP | HTE_OP | EQ_OP | DIFF_OP;
TYPE: BOOLEAN_TYPE | INT_TYPE;
BOOLEAN: TRUE | FALSE;
INTEGER: [0-9]+;
IDENTIFIER: [a-z][a-z_0-9]*;

TRUE: 'true';
FALSE: 'false';

BOOLEAN_TYPE: 'boolean';
INT_TYPE: 'int';

END_OF_STMT: ';';

OPENING_PARENTHESIS: '(';
CLOSING_PARENTHESIS: ')';

LT_OP:   '<';
LTE_OP:  '<=';
HT_OP:   '>';
HTE_OP:  '>=';
EQ_OP:   '=';
DIFF_OP: '<>';

ASSIGN_OP: ':=';
SEPARATOR: ',';

PLUS_OP: '+';
MINUS_OP: '-';
PRODUCT_OP: '*';
DIVISION_OP: '/';
