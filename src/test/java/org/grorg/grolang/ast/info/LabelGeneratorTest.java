package org.grorg.grolang.ast.info;

import java.util.Objects;

public class LabelGeneratorTest
{
    private class LabelImpl implements Label
    {
        private LabelGeneratorTest generator;
        private int label;

        private LabelImpl(LabelGeneratorTest generator, int label)
        {
            this.generator = generator;
            this.label = label;
        }

        @Override
        public boolean equals(Object o)
        {
            if (o == null || (!(o instanceof Label)))
            {
                return false;
            }
            Label that = (Label) o;
            return that.toString().equals(toString());
        }

        @Override
        public int hashCode()
        {
            return Objects.hash(toString());
        }

        @Override
        public String toString()
        {
            return "l" + generator.id + "_" + label;
        }
    }

    // The id of the last generator instance
    private static int instanceIndex = 0;
    private int id;
    private int index = 0;

    private LabelGeneratorTest(int id)
    {
        this.id = id;
    }

    public static LabelGeneratorTest newInstance()
    {
        return new LabelGeneratorTest(++instanceIndex);
    }

    public Label newLabel()
    {
        return new LabelImpl(this, this.index++);
    }
}
