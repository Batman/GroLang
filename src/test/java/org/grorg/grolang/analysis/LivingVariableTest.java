package org.grorg.grolang.analysis;

import io.vavr.collection.HashMap;
import io.vavr.collection.HashSet;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.grorg.grolang.AstFactory;
import org.grorg.grolang.ast.Identifier;
import org.grorg.grolang.ast.Program;
import org.grorg.grolang.ast.VariableName;
import org.grorg.grolang.ast.info.Label;
import org.grorg.grolang.ast.info.LabelGeneratorTest;
import org.grorg.grolang.lang.GroLang;
import org.grorg.grolang.lang.GroLangLexer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

final public class LivingVariableTest
{
    private static final VariableName x = new VariableName(new Identifier("x"));
    private static final VariableName y = new VariableName(new Identifier("y"));
    private static final VariableName z = new VariableName(new Identifier("z"));
    private LivingVariable livingVariable;
    private LabelGeneratorTest generator;

    @BeforeEach
    void init()
    {
        final String p = "program test begin int x, y, z;" +
                "x := 2; y := 4; x := 1;" +
                "if y > x then z := y else z := y * y;" +
                "x := z " +
                "end";
        GroLangLexer lexer = new GroLangLexer(CharStreams.fromString(p));
        CommonTokenStream tokenStream = new CommonTokenStream(lexer);
        GroLang parser = new GroLang(tokenStream);

        GroLang.ProgramContext ctx = parser.program();
        Program program = new AstFactory().visitProgram(ctx);
        Analyser analyser = new Analyser(program);
        this.livingVariable = new LivingVariable(analyser);
        this.generator = LabelGeneratorTest.newInstance();
    }

    @Test
    void livingVariable()
    {
        final AnalysisResult<VariableName> result = this.livingVariable.result();
        final Label L1 = this.generator.newLabel();
        final Label L2 = this.generator.newLabel();
        final Label L3 = this.generator.newLabel();
        final Label L4 = this.generator.newLabel();
        final Label L5 = this.generator.newLabel();
        final Label L6 = this.generator.newLabel();
        final Label L7 = this.generator.newLabel();

        assertEquals(HashMap.of(
                L1, HashSet.empty(),
                L2, HashSet.empty(),
                L3, HashSet.of(y),
                L4, HashSet.of(x, y),
                L5, HashSet.of(y),
                L6, HashSet.of(y),
                L7, HashSet.of(z)),
                result.entry());

        assertEquals(HashMap.of(
                L1, HashSet.empty(),
                L2, HashSet.of(y),
                L3, HashSet.of(x, y),
                L4, HashSet.of(y),
                L5, HashSet.of(z),
                L6, HashSet.of(z),
                L7, HashSet.empty()),
                result.exit());
    }
}
