package org.grorg.grolang.analysis;

import io.vavr.collection.HashSet;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.grorg.grolang.AstFactory;
import org.grorg.grolang.ast.*;
import org.grorg.grolang.ast.Integer;
import org.grorg.grolang.lang.GroLang;
import org.grorg.grolang.lang.GroLangLexer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

final class FreeVariableTest
{
    private static final VariableName x = new VariableName(new Identifier("x"));
    private static final VariableName y = new VariableName(new Identifier("y"));
    private static final VariableName z = new VariableName(new Identifier("z"));
    private static final BinaryArithmeticOperation aPlusB =
            new BinaryArithmeticOperation(
                    BinaryArithmeticOperation.Operator.PLUS,
                    new VariableName(new Identifier("a")),
                    new VariableName(new Identifier("b")));
    private static final BinaryArithmeticOperation aTimesB =
            new BinaryArithmeticOperation(
                    BinaryArithmeticOperation.Operator.PRODUCT,
                    new VariableName(new Identifier("a")),
                    new VariableName(new Identifier("b")));
    private static final BinaryArithmeticOperation aPlusOne =
            new BinaryArithmeticOperation(
                    BinaryArithmeticOperation.Operator.PLUS,
                    new VariableName(new Identifier("a")),
                    new Integer(1));

    @Test
    void freeVariableTest()
    {
        Assertions.assertEquals(HashSet.of(
                new VariableName(new Identifier("a")),
                new VariableName(new Identifier("b"))),
                FreeVariable.freeVariable(aPlusB));
        Assertions.assertEquals(HashSet.of(
                new VariableName(new Identifier("a")),
                new VariableName(new Identifier("b"))),
                FreeVariable.freeVariable(aTimesB));
        Assertions.assertEquals(HashSet.of(
                new VariableName(new Identifier("a"))),
                FreeVariable.freeVariable(aPlusOne));
    }

    @Test
    void freeVariableOnProgramTest()
    {
        final String p = "program test begin int x, y, z;" +
                "x := 2; y := 4; x := 1;" +
                "if y > x then z := y else z := y * y;" +
                "x := z " +
                "end";
        GroLangLexer lexer = new GroLangLexer(CharStreams.fromString(p));
        CommonTokenStream tokenStream = new CommonTokenStream(lexer);
        GroLang parser = new GroLang(tokenStream);

        GroLang.ProgramContext ctx = parser.program();
        Program program = new AstFactory().visitProgram(ctx);

        Assertions.assertEquals(
                HashSet.of(x, y, z),
                FreeVariable.freeVariable(program.body()));
    }
}
