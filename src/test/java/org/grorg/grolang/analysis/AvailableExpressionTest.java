package org.grorg.grolang.analysis;

import io.vavr.collection.HashMap;
import io.vavr.collection.HashSet;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.grorg.grolang.AstFactory;
import org.grorg.grolang.ProgramTest;
import org.grorg.grolang.ast.*;
import org.grorg.grolang.ast.info.Label;
import org.grorg.grolang.ast.info.LabelGeneratorTest;
import org.grorg.grolang.lang.GroLang;
import org.grorg.grolang.lang.GroLangLexer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

final public class AvailableExpressionTest
{
    private AvailableExpression availableExpression;
    private static final BinaryArithmeticOperation aPlusB =
            new BinaryArithmeticOperation(
                    BinaryArithmeticOperation.Operator.PLUS,
                    new VariableName(new Identifier("a")),
                    new VariableName(new Identifier("b")));
    private static final BinaryArithmeticOperation aTimesB =
            new BinaryArithmeticOperation(
                    BinaryArithmeticOperation.Operator.PRODUCT,
                    new VariableName(new Identifier("a")),
                    new VariableName(new Identifier("b")));
    private LabelGeneratorTest generator;

    @BeforeEach
    void init()
    {
        final String p = ProgramTest.p6;
        GroLangLexer lexer = new GroLangLexer(CharStreams.fromString(p));
        CommonTokenStream tokenStream = new CommonTokenStream(lexer);
        GroLang parser = new GroLang(tokenStream);

        GroLang.ProgramContext ctx = parser.program();
        Program program = new AstFactory().visitProgram(ctx);
        Analyser analyser = new Analyser(program);
        this.availableExpression = new AvailableExpression(analyser);
        this.generator = LabelGeneratorTest.newInstance();
    }


    @Test
    void availableExpression()
    {
        final AnalysisResult<ArithmeticExpression<?>> result = this.availableExpression.result();
        final Label L1 = this.generator.newLabel();
        final Label L2 = this.generator.newLabel();
        final Label L3 = this.generator.newLabel();
        final Label L4 = this.generator.newLabel();
        final Label L5 = this.generator.newLabel();

        assertEquals(HashMap.of(
                L1, HashSet.empty(),
                L2, HashSet.of(aPlusB),
                L3, HashSet.of(aPlusB),
                L4, HashSet.of(aPlusB),
                L5, HashSet.empty()),
                result.entry());

        assertEquals(HashMap.of(
                L1, HashSet.of(aPlusB),
                L2, HashSet.of(aPlusB, aTimesB),
                L3, HashSet.of(aPlusB),
                L4, HashSet.empty(),
                L5, HashSet.of(aPlusB)),
                result.exit());
    }
}
