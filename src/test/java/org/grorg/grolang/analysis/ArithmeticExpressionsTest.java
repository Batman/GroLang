package org.grorg.grolang.analysis;

import io.vavr.collection.HashSet;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.grorg.grolang.AstFactory;
import org.grorg.grolang.ProgramTest;
import org.grorg.grolang.ast.*;
import org.grorg.grolang.ast.Integer;
import org.grorg.grolang.lang.GroLang;
import org.grorg.grolang.lang.GroLangLexer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ArithmeticExpressionsTest
{
    private Program program;
    private static final BinaryArithmeticOperation aPlusB =
            new BinaryArithmeticOperation(
                    BinaryArithmeticOperation.Operator.PLUS,
                    new VariableName(new Identifier("a")),
                    new VariableName(new Identifier("b")));
    private static final BinaryArithmeticOperation aTimesB =
            new BinaryArithmeticOperation(
                    BinaryArithmeticOperation.Operator.PRODUCT,
                    new VariableName(new Identifier("a")),
                    new VariableName(new Identifier("b")));
    private static final BinaryArithmeticOperation aPlusOne =
            new BinaryArithmeticOperation(
                    BinaryArithmeticOperation.Operator.PLUS,
                    new VariableName(new Identifier("a")),
                    new Integer(1));

    @BeforeEach
    void init()
    {
        final String p = ProgramTest.p6;
        GroLangLexer lexer = new GroLangLexer(CharStreams.fromString(p));
        CommonTokenStream tokenStream = new CommonTokenStream(lexer);
        GroLang parser = new GroLang(tokenStream);

        GroLang.ProgramContext ctx = parser.program();
        this.program = new AstFactory().visitProgram(ctx);
    }

    @Test
    void testArithmeticExpressions()
    {
        Assertions.assertEquals(HashSet.of(aPlusB, aTimesB, aPlusOne),
                ArithmeticExpressions.arithmeticExpressions(program.body()));
    }
}
