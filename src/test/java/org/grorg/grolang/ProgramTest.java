package org.grorg.grolang;

public class ProgramTest
{
    public static final String p1 =
            "program p1\n" +
                    "begin\n" +
                    "int x;\n" +
                    "x := 100;\n" +
                    "while ( 0 < x ) do (\n" +
                    "  x := x -1\n" +
                    ")\n" +
                    "end";
    public static final String p2 =
            "program p2\n" +
                    "begin\n" +
                    "int x, y, z;\n" +
                    "y := x; z := 1;\n" +
                    "while (y > 1) do (\n" +
                    "  z := z*y; y := y -1\n" +
                    ");\n" +
                    "y := 0\n" +
                    "end\n";
    public static final String p3 =
            "program\n" +
                    "proc fib(int z, int u, res int v)\n" +
                    "  begin\n" +
                    "    if z < 3 then v := u+1" +
                    "    else (call fib(z -1, u, v); call fib(z -2, v, v))\n" +
                    "  end\n" +
                    "begin\n" +
                    "int x, y;\n" +
                    "call fib(x, 0, y)\n" +
                    "end\n";
    public static final String p4 =
            "program p4\n" +
                    "begin\n" +
                    "int x, y;\n" +
                    "int[10] t;\n" +
                    "x := 0; y := 0;\n" +
                    "while (x < 10) do (\n" +
                    "  y := y + t[x]; x := x +1;\n" +
                    ")\n" +
                    "end";
    public static final String p5 =
            "program p5\n" +
                    "begin\n" +
                    "int i, j;\n" +
                    "i := 2;\n" +
                    "y := 0;\n" +
                    "while (i + j < 20) do (\n" +
                    "    if (i >= 10) then \n" +
                    "        i := i + 4\n" +
                    "    else\n" +
                    "    (   i := i + 2;\n" +
                    "        j := j + 1    );\n" +
                    "    skip" +
                    ");\n" +
                    "t := 12\n" +
                    "end";
    public static final String p6 =
            "program p6\n" +
                    "begin\n" +
                    "int x, y, a, b;\n" +
                    "x := a+b;\n" +
                    "y := a * b;\n" +
                    "while y > a + b do\n" +
                    "( a:=a+1;\n" +
                    "  x := a + b)\n" +
                    "end";
}